# 1.2.1

- Tipo fixes

# 1.2.0

- Add fullscreen button to articles
- Avoid doble reload of feeds when scrolling down for the first time

# 1.1.5

- Fix logout of tt-rss accounts when no connection available

# 1.1.3

- Fix style

# 1.1.2

- Fix style for firefoxos < 2.0

# 1.1.1

- Bigger select zone for read/unread flag

# 1.1.0

- Add Selfoss API support
- Configurable option to mark as read when opening an article

# 1.0.4

- Fix counter
- Open the url in a popin in case of self signed certificate, to allow the user to accept it.

# 1.0.3

- Mark as read latest item when only 1 item is displayed
- Fix Feedly empty summary content

# 1.0.2

- Many thanks to M.B. for the feedback!
- Better mark as read while scrolling
- Add count unread articles in title
- Configuration to customize number of character displayed on summary

# 1.0.1

- Auto mark as read while scrolling (thanks to M.B. for the idea)

# 0.4.9

- add vibrations on click

# 0.4.8

- add progress bar
- add rate app url in about section
- clear feed list on update

# 0.4.6

- fix unread item count in notifications
- selected label has a specific background color to identify
- auto go to next folder when clicking on read all

# 0.4.5

- Added "owncloud news" support

# 0.4.4

- Fix websms/sms share link

# 0.4.3

- Add open article in browser link after content
- fix feedly links
- remove select icons

# 0.4.1

- Adv settings: max search items
- Fix feedly links


# 0.4.0

- Advanced settings: hide regex
- UX style update
- Animated loadigin icon
- Auto scroll to last opened, when closing item
- Bug fixes (readall function, ...)

# 0.3.4

- Fix toggle read button

# 0.3.3

- fix feedly readall

# 0.3.2

- Always display main labels
- Search in feeds
- Fix feedly login on desktop
- Group settings by pane
- New settings design
- Button labels
- Error messages on tt-rss login

# 0.3.1

- Landscape design
- Add language settings
- Reoder settings page
- New design for read items
- Add read/unread flag button
- Added unsubscribe method on feedly
- Bug fixes



# 0.3.0

- New settings interface
- Add about section in settings
- Refresh token for feedly
- Display user/email and API of logged user

# 0.2.7

- Ux Update

# 0.2.6

- Font family for desktop/firefox mobile
- Fix feedly published date
- Fix feedly canonical href on some feeds
- Use embedded iframe for feedly login (avoid use of window.open on Firefox Android and Desktop)
- Fix bug "undefined" text on title when not loggedin


# 0.2.5

- Feedly integration!
- View first word of content in main list
- Add title in main list + counter

# 0.2.4

- View first word of content in main list

# 0.2.3

- UX Update
- Bigger click zone
- Promise polyfill for FirefoxOS 1.3 compatibility

# 0.2.2

- Redesign main items

# 0.2.1

- Fix empty buttons text

# 0.2.0

- Delete feeds
- Bold feed title when displayed fullscreen
- Add feed date in main list
- Use css icons for checkbox button 

# 0.1.9

- Fix save update time
- Share button trigger open mail popup

# 0.1.7

- Added Notifications
- Added scroll to top button
- Bigger font size for feed content

# 0.1.1

- add swipe events
- add date on fullscreen items

# 0.1.0

- Init version


