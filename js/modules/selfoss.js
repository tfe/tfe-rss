var Selfoss= function()
{
    var self=this;
    this.type='selfoss';
    this.typename='SelfOSS';
    this.username = null;
    this.password = null;
    this.disable_liked = true;
    this.disable_readall=true;

    this.all_id  = 'FEED:-4';
    this.starred_id  = 'FEED:-3';


    // Init XHR object
    this.xhr = new XMLHttpRequest({ mozSystem: true });

};


Selfoss.prototype.create_form = function()
{
    var form = document.createElement('form');
    form.id=this.type;
    form.innerHTML=
        '<p class="form_text loggedin">'+translate('connected_with')+' '+this.typename+' /  <span id="loggedin_user"></span></p>'+
        '<p class="form_text loggedout" value="'+translate('enter_account_information')+'"></p>'+
        '<p>'+
        '<span class="form_help loggedout">'+translate('form_url_selfoss_help')+'</span>'+
        '<input type="url" name="url" placeholder="'+translate('form_url_selfoss')+'" required>'+
        '<input type="text" name="user" placeholder="'+translate('form_user')+'" required>'+
        '<input type="password" name="password" placeholder="'+translate('form_password')+'" required>'+
        '</p>'+
        '<p><button class="login_link bb-button bb-recommend">'+translate('login_link')+'</button></p>'+
        '<p><button class="logout_link bb-button bb-danger">'+translate('logout_link')+'</button></p>';
    return form;
};

Selfoss.prototype.init = function()
{
    var self=this;
    if(!self.inited)
    {
        this.form = this.create_form();
        this.login_link = this.form.querySelector('.login_link');
        this.logout_link = this.form.querySelector('.logout_link');
        this.user = this.form.querySelector('input[name=user]');
        this.url = this.form.querySelector('input[name=url]');
        this.password = this.form.querySelector('input[name=password]');
        // Bind buttons
        this.form.addEventListener('submit', vibrate.button.bind(vibrate));
        this.form.addEventListener('submit', function(e) { return self.login(e); }, false);

        this.login_link.addEventListener('submit', vibrate.button.bind(vibrate));
        this.login_link.addEventListener('submit', function(e) { return self.login.bind(self)(e); }, false);

        this.logout_link.addEventListener('click', vibrate.button.bind(vibrate));
        this.logout_link.addEventListener('click', function(e) { return self.logout.bind(self)(e); }, false);
        settings.add_api(this.type, this.typename, this.form);
    }
    self.inited=1;

    return Promise.all([
            this.initDb()
    ]);
};

Selfoss.prototype.logout = function(e)
{
    this.deleteAccount(this.loggedout.bind(this));
    settings.logout();
    e.preventDefault();
};

Selfoss.prototype.login= function(e)
{
    var self=this;
    if(this.form.checkValidity())
    {
        e.preventDefault();

        this._login(this.user.value,this.password.value)
            .then(
                settings.init_accounts.bind(settings),
                function()
                {
                    settings.alert(translate('login_fail'));
                });
    }
    else
    {
        e.preventDefault();
        return false;
    }
};

Selfoss.prototype.initDb = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var request;
        try
        {
            request = indexedDB.open('selfoss_db',3.0);
        }
        catch(err)
        {
            console.error('error open db',this);
        }
        request.onsuccess = function (e) {
            self.db = e.target.result;
            ok();
        };
        request.onerror = function (e) {
            console.log(e);

            request = indexedDB.deleteDatabase('selfoss_db');
            request.onsuccess= function()
            {
                self.initDb().then(ok, reject);
            };
            request.onerror= function()
            {
                console.error('error init db Files');
                settings.alert(translate('error_open_db', 10000));
                reject();
            };
        };
        request.onupgradeneeded = function (e) {
            self.db = e.target.result;

            if (self.db.objectStoreNames.contains("labels")) {
                self.db.deleteObjectStore("labels");
            }
            if (self.db.objectStoreNames.contains("accounts")) {
                self.db.deleteObjectStore("accounts");
            }
            if (self.db.objectStoreNames.contains("feeds")) {
                self.db.deleteObjectStore("feeds");
            }
            if (self.db.objectStoreNames.contains("counts")) {
                self.db.deleteObjectStore("counts");
            }
            if (self.db.objectStoreNames.contains("items")) {
                self.db.deleteObjectStore("items");
            }

            var objectStore = self.db.createObjectStore('accounts', { keyPath: 'id', autoIncrement: true });

            objectStore = self.db.createObjectStore('feeds', { keyPath: 'id', autoIncrement: true });

            objectStore = self.db.createObjectStore('counts', { keyPath: 'id', autoIncrement: true });

            objectStore = self.db.createObjectStore('items', { keyPath: 'id', autoIncrement: true });

            objectStore = self.db.createObjectStore('labels', { keyPath: 'id', autoIncrement: true });
            objectStore.createIndex("sortid", "sortid", { unique: false });
            objectStore.createIndex("id", "id", { unique: false });
        };
    });

};

// Methodes
Selfoss.prototype._login = function(user, password)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        settings.alert(translate('connecting_to_the_account'));
        var r = self.xhr;
        r.open("GET", self.url.value+'/items?username='+encodeURIComponent(user)+'&password='+encodeURIComponent(password), true);
        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                var data;
                try
                {
                    data = JSON.parse(r.responseText);
                }
                catch(err)
                {
                    settings.alert('ERROR: '+r.responseText);
                    reject();
                    return;
                }
                self.create_account(self.url.value, user, password)
                    .then(ok);
            }
        };

        // Send xhr request
        r.send(JSON.stringify({ op : 'login', user: user, password: password }));
    });
};

Selfoss.prototype.loggedin = function()
{
    this.user.value = this.getUser();
    this.url.value = this.getHost();
    this.form.querySelector('#loggedin_user').innerHTML= this.getUser();
    this.form.classList.add("loggedin");
    this.user.disabled=true;
    this.url.disabled=true;
};

Selfoss.prototype.loggedout = function()
{
    this.form.querySelector('#loggedin_user').innerHTML= '';
    this.form.classList.remove("loggedin");
    this.user.disabled=false;
    this.url.disabled=false;
};

Selfoss.prototype.create_account = function(host, user, password)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var transaction = self.db.transaction([ 'accounts' ], 'readwrite');
        //Create the Object to be saved i.e. our Note
        var value = {};
        value.host = host;
        value.user = user;
        value.password = password;

        var accounts = transaction.objectStore('accounts');
        var request = accounts.add(value);
        request.onsuccess = function (e) {
            ok();
        };
        request.onerror = function (e) {
            reject();
        };
    });
};

Selfoss.prototype.getAccount = function(callback)
{
    var account = null;
    var self=this;

    var transaction = this.db.transaction([ 'accounts' ]);
    var dbaccounts = transaction.objectStore('accounts');
    var received=false;

    // open a cursor to retrieve all items from the 'notes' store
    dbaccounts.openCursor().onsuccess = function (e) {
        var cursor = e.target.result;
        if (cursor) {
            received=true;
            self.account = cursor.value;
            self.host =  self.account.host;
            cursor.continue();
            // Test connection
            var url = self.host+'/api/';
            callback(self.account);
        }
        else if(!received)
        {
            callback(null);
        }
    };
};

Selfoss.prototype.getUser = function()
{
    return this.account  ? this.account.user : '';
};
Selfoss.prototype.getHost = function()
{
    return this.account  ? this.account.host : '';
};




Selfoss.prototype.deleteAccount = function(callback)
{
    if(this.account)
    {
        var request = this.db.transaction(["accounts"], "readwrite")
            .objectStore("accounts")
            .delete(this.account.id);
        request.onsuccess = function(event) {
            callback();
        };
        this.account=null;
    }
};

Selfoss.prototype.isLoggedIn = function(callback)
{
    if(!this.account)
    {
        return false;
    }
    return true;
};

Selfoss.prototype._query = function(method,url,data,callback)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        // Init XHR object
        var r = new XMLHttpRequest({ mozSystem: true });

        if(self.account)
        {
            if(method=='GET')
            {
                url+= (url.indexOf('?')===-1 ? '?' : '&')+
                    'username='+encodeURIComponent(self.account.user)+
                    '&password='+encodeURIComponent(self.account.password);
            }
            else
            {
                data+='&username='+encodeURIComponent(self.account.user)+'&';
                data+='&password='+encodeURIComponent(self.account.password)+'&';
            }
        }
        r.open(method, url, true);

        if(!data || typeof data ==='string')
        {
            r.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        }
        else
        {
            data = JSON.stringify(data);
            r.setRequestHeader("Content-type","application/json");
        }

        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                if(r.status == 200)
                {
                    return ok(r.responseText);
                }
                else
                {
                    return reject(null);
                }
            }
        };
        r.send(data);
    });
};



Selfoss.prototype.updateSubscriptionList = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var url = self.host+'/sources/list';
        self._query("GET", url)
            .then(function(text)
            {
                var data = JSON.parse(text);
                if(data)
                {
                    self.addSubscriptions(data)
                        .then(ok, reject);
                }
                else
                {
                    reject();
                }   
            }, reject);
    });
};

Selfoss.prototype.addSubscriptions = function(subscriptions)
{
    var self=this;
    self.tag_subscriptions=[];
    return new Promise(function(ok, reject)
    {
        var transaction_feeds = self.db.transaction([ 'feeds' ], 'readwrite');
        transaction_feeds.oncomplete= ok;
        transaction_feeds.onerror= reject;

        // Remove previous feeds
        var allfeeds = transaction_feeds.objectStore('feeds');
        allfeeds.clear();

        //Create the Object to be saved i.e. our Note
        subscriptions.forEach(function(feed)
        {
            var feeds = transaction_feeds.objectStore('feeds');
            var tags = feed.tags.split(/,/g);
            feed.category = tags ? 'CAT:'+tags[0] : '';
            tags.forEach(function(tag)
            {
                if(!self.tag_subscriptions[tag])
                {
                    self.tag_subscriptions[tag]=[];
                }
                self.tag_subscriptions[tag].push(feed.id);
            });

            var request = feeds.add(feed);
        });
    });
};

Selfoss.prototype.updateLabelsList = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var url = self.host+'/sources/list';
        self._query("GET", url)
            .then(function(text)
            {
                var data = JSON.parse(text);
                if(data)
                {
                    self.addLabels(data)
                        .then(ok, reject);
                }
                else
                {
                    reject();
                }
            }, reject);
    });
};

Selfoss.prototype.addLabels = function(labels)
{
    var self=this;
    self.labels=[];
    return new Promise(function(ok, reject)
    {
        var transaction_labels = self.db.transaction([ 'labels' ], 'readwrite');
        //Create the Object to be saved i.e. our Note
        transaction_labels.oncomplete= ok;
        transaction_labels.onerror= reject;

        // Remove previous labels
        var alllabels = transaction_labels.objectStore('labels');
        alllabels.clear();

        labels.forEach(function(data)
        {
            var tags = data.tags.split(/,/g);
            tags.forEach(function(tag)
            {
                var labels = transaction_labels.objectStore('labels');
                data.label = tag;
                data.id ='CAT:'+tag;
                var request = labels.put(data);
                self.labels.push(data);
            });
        });
    });
};

Selfoss.prototype.updateCount = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var url = self.host+'/sources/stats';
        self._query("GET", url)
            .then(function(text)
            {
                var data = JSON.parse(text);
                if(data)
                {
                    self.addCounts(data)
                        .then(ok, reject);
                }
                else
                {
                    reject();
                }
            }, reject);
    });
};

Selfoss.prototype.addCounts = function(counts)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var transaction_counts = self.db.transaction([ 'counts' ], 'readwrite');
        //Create the Object to be saved i.e. our Note
        transaction_counts.oncomplete= ok;
        transaction_counts.onerror= reject;

        // Remove previous counts
        var allcounts = transaction_counts.objectStore('counts');
        allcounts.clear();

        var total=0;
        var values=[];
        counts.forEach(function(data)
        {
            data.unread = parseInt(data.unread, 10);
            total+=data.unread;

            var counts_save = transaction_counts.objectStore('counts');
            var save_data={};
            save_data.count = data.unread;
            save_data.id=data.id;
            values[data.id] = data.unread;
            var request = counts_save.add(save_data);
        });

        // add global count
        var counts_save = transaction_counts.objectStore('counts');
        var save_data={};
        save_data.count = total;
        save_data.id=self.all_id;
        var request = counts_save.add(save_data);

        // add cat sums
        if(self.tag_subscriptions)
        {
            for(var label in self.tag_subscriptions)
            {
                var counts_save = transaction_counts.objectStore('counts');
                var save_data={};
                save_data.id='CAT:'+label;
                sum=0;
                self.tag_subscriptions[label].forEach(function(feed)
                {
                    sum += values[feed];
                });
                save_data.count = sum;
                var request = counts_save.put(save_data);
            }
        }
    });
};

Selfoss.prototype.fullupdate = function()
{
    return Promise.all([
            this.updateSubscriptionList(),
            this.updateLabelsList(),
            this.updateCount()
    ]);
};

Selfoss.prototype.getFeeds = function()
{
    var self=this;
    self.feed_cache = {};
    return new Promise(function(ok, reject)
    {
        var feeds = [];

        var transaction = self.db.transaction([ 'feeds' ]);
        var dbfeeds = transaction.objectStore('feeds');

        // open a cursor to retrieve all items from the 'notes' store
       var c = dbfeeds.openCursor();
       c.onsuccess = function (e) {
            var cursor = e.target.result;
            if (cursor) {
                feeds.push(cursor.value);
                self.feed_cache[cursor.value.id] = cursor.value;
                cursor.continue();
            }
            else
            {
                ok(feeds);
            }
        };
       c.onerror = reject;
    });
};

Selfoss.prototype.getLabels = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var labels = [];

        var transaction = self.db.transaction([ 'labels' ]);
        var dblabels = transaction.objectStore('labels');
        var index = dblabels.index('id');

        // open a cursor to retrieve all items from the 'notes' store
       var c = index.openCursor();
       c.onsuccess = function (e) {
            var cursor = e.target.result;
            if (cursor) {
                labels.push(cursor.value);
                cursor.continue();
            }
            else
            {
                ok(labels);
            }
        };
       c.onerror = reject;
    });
};

Selfoss.prototype.getCounts = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var counts = [];

        var transaction = self.db.transaction([ 'counts' ]);
        var dbcounts = transaction.objectStore('counts');

        // open a cursor to retrieve all items from the 'notes' store
       var c = dbcounts.openCursor();
       c.onsuccess = function (e) {
            var cursor = e.target.result;
            if (cursor) {
                counts.push(cursor.value);
                cursor.continue();
            }
            else
            {
                ok(counts);
            }
        };
       c.onerror = reject;
    });
};

Selfoss.prototype.getItems = function(id, viewRead, next, limit)
{
    var self=this;
    // if reloading a category, empty cache to avoid too much mem use
    if(!next || !this.item_cache)
    {
        this.item_cache={};
    }

    return new Promise(function(ok, reject)
    {
        if(!next) { next= 0; }
        else
        {
            next=parseInt(next,10);
        }
        if(!limit) { limit= 20; }
        var items=[];
        var ids=[];
        var url = self.host+'/items?';

        var is_cat= /CAT/.test(id);
        var url_id = id;
        console.log('id ',id);

        var url_type = "3";
        if(is_cat)
        {
            url += 'tag='+encodeURIComponent(id.replace('CAT:',''))+'&';
        }
        else if(/^(FEED:)?\d+$/.test(id))
        {
            url += 'source='+encodeURIComponent(id.replace('FEED:',''))+'&';
        }

        if(url_id==self.starred_id)
        {
            url += 'type=starred&';
        }
        else if(!viewRead)
        {
            url += 'type=unread&';
        }

        if(id==this.all_id)
        {
            id='';
        }

        url+= 
            'items=20&'+
            (next ? 'offset='+next+'&' : '')
        ;
        self._query("GET", url)
            .then(function(text)
            {
                var received = JSON.parse(text);
                if(received)
                {
                    var data;
                    data = { continuation: (received.length==20 ? next+20 : null), items: [] }; 
                    console.log('data',data);
                    Array.forEach(received, function(item)
                    {
                        var date =  item.datetime.match(/(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)/);

                        self.item_cache[item.id] = item.feedId+'/'+item.guidHash;
                        var newitem= {
                            readall_key : item.id,
                            category:  id,
                            id: item.id,
                            published: (new Date(date[1], parseInt(date[2],10)-1, date[3],null, date[4], date[5], date[6])).getTime()/1000,
                            updated: (new Date(date[1], parseInt(date[2],10)-1, date[3],null, date[4], date[5], date[6])).getTime()/1000,
                            title: item.title,
                            summary : { content: item.content },
                            canonical: [ { href: item.link } ],
                            unread: parseInt(item.unread,0),
                            origin: { title:  item.sourcetitle },
                            starred: parseInt(item.starred,10),
                            liked: false
                        };
                        data.items.push(newitem);
                    });
                    ok(data);
                }
                else
                {
                    reject();
                }
            }, reject);
    });
};

Selfoss.prototype.markRead= function(item_id, state)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        // unmark is not compatible with multiple ids
        if(!state)
        {
            console.log(1);
            var url = self.host+'/unmark/'+item_id
            console.log('unmark ',url);
            self._query("POST", url, 'item='+item_id);
            ok();
        }
        else
        {
            // Group mark read requests
            if(!self.markread_ids)
            {
                self.markread_ids = [];
            }
            self.markread_ids.push(item_id);

            window.clearTimeout(self.markread_timer);
            self.markread_timer = window.setTimeout(function()
            {
                var url = self.host+'/'+(state ? 'mark' :'unmark')+'/';
                var ids = [];
                self.markread_ids.forEach(function(id)
                {
                    ids.push('ids[]='+id);
                });

                console.log('ids ',ids);
                self.markread_ids=[];
                self._query("POST", url, ids.join('&'));
            },  1500);
            ok();
        }
    });
};

Selfoss.prototype.deleteFeed= function(item_id)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var url = self.host+'/source/'+item_id;
        self._query("DELETE", url)
            .then(function(text)
            {
                ok(text);
            }, reject);
    });
};

Selfoss.prototype.markLike= function(item_id, state)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        reject();
    });
};

Selfoss.prototype.markStar= function(item_id, state)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var url = self.host+'/'+(state ? 'starr' :'unstarr')+'/'+item_id;
        self._query("POST", url)
            .then(function(text)
            {
                ok(text);
            }, function()
            {
                reject()
            });
    });
};

Selfoss.prototype.readAll= function(item_id, ts)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        reject();
    });
};


Selfoss.prototype.addFeed= function(url)
{
    var self=this;
    var addurl = url;
    return new Promise(function(ok, reject)
    {
        var query_url = self.host+'/source/';
        var data = { url: url, folderId: 0};

        self._query("POST", query_url, data)
            .then(function(text)
            {
                var data = JSON.parse(text);
                if(data.error)
                {
                    reject(data);
                }
                else
                {
                    ok(data);
                }
            }, reject);
    });
};

